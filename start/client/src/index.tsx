import React from "react";
import ReactDOM from "react-dom";
import { ApolloClient, ApolloProvider, gql, useQuery } from "@apollo/client";

import Pages from "./pages";
import injectStyles from "./styles";
import { cache } from "./cache";
import Login from "./pages/login";

// You can extend a GraphQL type that's defined in
// another location to add fields to that type.
export const typeDefs = gql`
  extend type Query {
    isLoggedIn: Boolean!
    cartItems: [ID!]!
  }
`;

const IS_LOGGED_IN = gql`
  query IsUserLoggedIn {
    isLoggedIn @client
  }
`;

function IsLoggedIn() {
  const { data } = useQuery(IS_LOGGED_IN);
  return data.isLoggedIn ? <Pages /> : <Login />;
}

const client = new ApolloClient({
  cache,
  uri: "http://localhost:4000/graphql",
  typeDefs,
  headers: {
    authorization: localStorage.getItem("token") || "",
  },
});

injectStyles();

ReactDOM.render(
  <ApolloProvider client={client}>
    <IsLoggedIn />
  </ApolloProvider>,
  document.getElementById("root")
);
