const { ApolloServer } = require("apollo-server");
const isEmail = require("isemail");
require("dotenv").config();

const typeDefs = require("./schema");
const resolvers = require("./resolvers");
const { createStore } = require("./utils");
const LaunchAPI = require("./datasources/launch");
const UserAPI = require("./datasources/user");
const { User } = require("./resolvers");

const store = createStore();

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => ({
    launchAPI: new LaunchAPI(),
    userAPI: new UserAPI({ store }),
  }),
  context: async ({ req }) => {
    /**
     * The context function defined above is called once for every GraphQL
     * operation that clients send to our server. The return value of
     * this function becomes the context argument that's passed to
     * every resolver that runs as part of that operation.
     */
    const auth = (req.headers && req.headers.authorization) || "";
    const email = Buffer.from(auth, "base64").toString("ascii");

    if (!isEmail.validate(email)) return { user: null };

    const users = await store.users.findOrCreate({ where: { email } });
    const user = (users && users[0]) || null;

    return { user: { ...user.dataValues } };
  },
});

server.listen().then(() => {
  console.log(`
        Server is running!
        Listening on port 4000
        Explore at https://studio.apollographql.com/sandbox
    `);
});
